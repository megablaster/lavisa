<?php get_header();?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-7">
            <img src="images/curriculum.png" class="img-fluid">
        </div>
        <div class="col-md-5 d-flex align-items-center pl-3 pr-3">
            <ul class="list-unstyled text-muted">
                <li><h2>Currículum</h2></li>
                <li>Control de Fluidos en México desde 1932 LAVISA, S. DE R.L. DE C.V. 
                Empresa especialista en Control de Fluidos en México desde 1932, 
                pone a sus órdenes para sus próximos proyectos y adquisiciones: 
                Tubería, Válvulas, Conexiones y Accesorios en acero al carbón, 
                acero inoxidable, hastelloy, alloy 20, inconel, monel, titanio y 
                bronce; Materiales estandar y de fabricación especial, nuevos, 
                genuinos, de reciente fabricación, contando con certificados de 
                calidad auténticos, garantías de fábrica, marcas de prestigio 
                nacionales e internacionales, por lo que usted contará con una 
                perfecta trazabilidad en sus materiales, brindándole confianza 
                y garantía para sus proyectos y mantenimientos.</li>
                <li>Lavisa® le ofrece la posibilidad de obtener materiales con 
                certificaciones internacionales como ABS, dando cumplimiento a 
                las normas requeridas para obras certificadas.</li>
                    <li>Lavisa® cuenta actualmente con 5 Sucursales: México, Querétaro, 
                Guadalajara, Tampico, Coatzacoalcos y dos oficinas de ventas, 
                una en la ciudad de Villahermosa y otra en el Puerto de Veracruz.</li>
                </ul>
        </div>
    </div>
</div>
    <?php get_footer(); ?>
                    
