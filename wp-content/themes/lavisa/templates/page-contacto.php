<?php
/* Template Name: Contacto
*/ 

get_header(); 

the_post();

?>
<section>
    <div class="container-fluid">
        <div class="row row d-flex align-items-center justify-content-center fondo-patron">
            <div class="col-md-4 text-muted pl-5">
                <?php the_content();?>
            </div>
            <div class="col-md-8 no-gutters text-right">
                <?php 
                    if (has_post_thumbnail()){ 
                    $imagen = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID),'full'); 
                    if (!empty($imagen)){
                            $src = $imagen['0'];
                    }else{
                        $src = get_template_directory_uri() . '/img/no-image.jpg';
                    }
                } ?>
                <img src="<?php echo $src;?>" class="img-fluid">
            </div>
            <div class="col-md-6 no-gutters">
                <div class="embed-responsive embed-responsive-16by9">
                    <?php the_field('mapa'); ?>
                </div>
            </div>
            
            <div class="col-md-6 d-flex align-items-center justify-content-center text-muted fondo-patron">
                <div class="row d-flex align-items-center justify-content-center ">
                    
                    <div class="col-md-4 pl-2 pr-2 ">
                        <?php the_field('texto_form'); ?>
                    </div>
                    <div class="offset-1 col-md-7 pl-2 pr-2">     
                            <?php 
                                $shortcode =  get_field('form');
                                 echo do_shortcode($shortcode); 
                            ?>
                    </div>
                </div>
               
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>