<?php
/* Template Name: certificaciones
*/ 

get_header(); 

the_post();

?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4 d-flex align-items-center no-gutters justify-content-center text-center">
            
            <img src="<?php the_field('imagen_principal'); ?>" class="img-fluid">
        </div>
        <div class="col-md-8 no-gutters">
            <div class="owl-carousel owl-theme">
                <?php 
                    if( have_rows('carousel') ): 
                      while( have_rows('carousel') ): the_row(); 
                      ?>

                  <div class="item">
                    <img src="<?php the_sub_field('imagen'); ?>" class="img-fluid">
                </div>

                <?php
                    endwhile; 
                    endif;
                    wp_reset_query();
                ?> 
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>