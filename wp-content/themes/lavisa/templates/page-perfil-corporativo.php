<?php
/* Template Name: Perfil corporativo
*/ 

get_header(); 

the_post();

?>
    <div class="container-fluid h-100 no-gutters">
            <div class="owl-carousel owl-theme">
                    
                <?php 
                    if( have_rows('slide') ): 
                      while( have_rows('slide') ): the_row(); 
                      ?>

                       <div class="item altura-general" style="background-image: url('<?php the_sub_field('imagen'); ?>'); background-size: cover;">
                            <div class="row d-flex h-100 align-items-center">
                                <div class="col-md-4 px-5 text-muted text-justify">
                                    <?php the_sub_field('texto'); ?>
                                </div>
                            </div>
                        </div>

                <?php
                    endwhile; 
                    endif;
                    wp_reset_query();
                ?> 

            </div>
        </div>
<?php get_footer(); ?>