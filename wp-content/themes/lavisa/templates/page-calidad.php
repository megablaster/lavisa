<?php
/* Template Name: servicio-calidad
*/ 

get_header(); 

the_post();

?>


    <div class="container-fluid">
    <div class="row">
        <div class="col-md-4 d-flex align-items-center no-gutters justify-content-center text-center">
            <img src="<?php the_field('imagen_principal'); ?>" alt="Calidad lavisa" class="img-fluid">
        </div>
        <div class="col-md-8 no-gutters">
            <div class="owl-carousel owl-theme">
                

                  <div class="item">
                    <img src="<?php the_field('imagen_secundaria'); ?>" class="img-fluid">
                </div>
                 <div class="item">
                    <img src="<?php the_field('imagen_secundaria'); ?>" class="img-fluid">
                </div>

            
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
