<?php
/* Template Name: Productos
*/ 

get_header(); 

the_post();

?>

<section class="fondo-producos altura-general">
    <div class="tab-content" id="myTabContent">

        <? if( have_rows('producto_adicionales') ):
                    $counterTab = 0;
                    while ( have_rows('producto_adicionales') ) : the_row();
                        $activeTab =  $counterTab == 0 ? 'show active': 'none'; ?>
                        <div class="tab-pane fade <?php echo $activeTab ?>" id="tab-<?php echo $counterTab; ?>" role="tabpanel" aria-labelledby="tab-<?php echo $counterTab; ?>">

                            <div class="container-fluid h-100">
                                <div class="row h-100 align-items-center justify-content-center">
                                    <div class="col-md-6 text-center">
                                        <?php 
                                                if (has_post_thumbnail()){ 
                                                $imagen = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID),'full'); 
                                                if (!empty($imagen)){
                                                        $src = $imagen['0'];
                                                }else{
                                                    $src = get_template_directory_uri() . '/img/no-image.jpg';
                                                }
                                            } ?>

                                        <img src="<?php echo $src; ?>" class="img-fluid w-50" width="40">
                                    </div>
                                    <div class="col-md-5  text-muted">
                                         <?php the_content();?>
                                    </div>
                                </div>
                            </div>

                        </div>
                <?
                    $counterTab++;
                    endwhile;
                endif;
                 wp_reset_postdata(); 
                ?>  

    </div>

    <div class="container">
        <div class="row">
            <ul class="nav nav-tabs" id="myTab" role="tablist">  
                
                <?  if( have_rows('producto_adicionales') ):
                        $counterLinks = 0;
                        while ( have_rows('producto_adicionales') ) : the_row(); 
                            $activeLink =  $counterLinks == 0 ? 'active': '';   
                            $selected = $counterLinks == 0 ? 'true': 'false';

                            /*por si se hacen tabs #tab-<?php echo $counterLinks; class="nav-link <?php echo $activeLink ?>" id="tab-<?php echo $counterLinks; ?>" data-toggle="tab" role="tab" aria-controls="tab-<?php echo $counterLinks; ?>" aria-selected="<?php echo $selected ?>"
                            */
                            ?>
                             <li class="nav-item">    

                                    <a  class="nav-link"  href="<?php the_sub_field('liga'); ?>">
                                        <img src="<?php the_sub_field('imagen'); ?>" width="150" class="img-fluid">   
                                    </a>
                                </li>


                    <?
                        $counterLinks++;
                        endwhile;
                    endif;
                    wp_reset_postdata(); 
                ?>

            </ul>
        </div>
    </div>

</section>
<?php get_footer(); ?>