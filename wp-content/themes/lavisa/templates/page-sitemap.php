<?php
/* Template Name: Mapa de sitio
*/ 

get_header(); 

the_post();

?>
<section class="altura-general">
	<div class="container h-100">
		<h2 class="text-center">MAPA DE SITIO</h2>
			<div class="row h-100 justify-content-start p-5">

			
				<?php 
				    if( have_rows('secciones') ): 
				      while( have_rows('secciones') ): the_row(); 
				      ?>
				      <div class="col-md-3 col-sm-12">
					<h4><?php the_sub_field('titulo'); ?></h4>
						<ul>
							
							<?php 
							    if( have_rows('ligas') ): 
							      while( have_rows('ligas') ): the_row(); 
							      	$ligas = get_sub_field('liga');
							      ?>

							    <li><a href="<?php echo $ligas['url']; ?>" class="text-muted" target="_blank">
							    	<?php echo $ligas['title']; ?> </a>
							    </li>

							<?php
							    endwhile; 
							    endif;
							    wp_reset_query();
							?> 

						</ul>
					</div>
				<?php
				    endwhile; 
				    endif;
				    wp_reset_query();
				?> 

				
			


		</div>
	</div>
</section>
<?php get_footer(); ?>