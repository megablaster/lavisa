<?php
/* Template Name: pruebas
*/ 

get_header(); 

the_post();

?>

<section class="altura-general">
    <div class="container-fluid h-100 d-flex flex-column">
        <div class="container d-flex h-100">
            <div class="row justify-content-center align-self-center">
                <div class="col-md-12 text-center">
                        <h3 class="text-muted">Pruebas</h3>
                        <div class="row">
                        <?php 
                            if( have_rows('prueba') ): 
                              while( have_rows('prueba') ): the_row(); 
                              ?>

                            <div class="col-md-3 mt-4 prueba">
                                <img src="<?php the_sub_field('imagen_prueba'); ?>" alt="" class="img-fluid">
                            </div>

                       <?php
                            endwhile; 
                            endif;
                            wp_reset_query();
                        ?> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>