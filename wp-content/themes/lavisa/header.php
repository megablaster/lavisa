<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url');?>/css/bootstrap.min.css">
		<title><?php wp_title('|',true,'right'); ?> <?php bloginfo('name'); ?></title>
		<link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url');?>/css/generals.css">
		<link href="https://fonts.googleapis.com/css?family=Gudea&display=swap" rel="stylesheet">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="<?php bloginfo('template_url');?>/images/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php bloginfo('template_url');?>/images/favicon.ico" type="image/x-icon">
		<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,700&display=swap" rel="stylesheet">
	</head>
	

	<body id="SitioLavisa">
		<header>

			<nav class="navbar navbar-expand-lg navbar-light bg-white">
					<div class="container-fluid px-4">
						<a class="navbar-brand" href="<?php bloginfo('wpurl');?>">
							<img src="<?php the_field('logo', 'option'); ?>" alt="Lavisa® Control de Fluidos en México desde 1932.">
						</a>
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"></span>
						</button>
						<div class="collapse navbar-collapse" id="navbarNav">
							<ul class="navbar-nav ml-auto">
							  <?php
						   wp_nav_menu([
						     'menu'            => 'primary',
						     'theme_location'  => 'primary',
						     'container'       => 'div',
						     'container_id'    => 'bs4navbar',
						     'container_class' => 'mx-3',
						     'menu_id'         => false,
						     'menu_class'      => 'navbar-nav ml-auto',
						     'depth'           => 2,
						     'fallback_cb'     => 'bs4navwalker::fallback',
						     'walker'          => new bs4navwalker()
						   ]);
	                       ?>  
	                         <a href="#"><img src="<?php bloginfo('template_url');?>/images/bandera_mexico.png" alt="" width="40"></a>
							 <a href="#"><img src="<?php bloginfo('template_url');?>/images/bandera_usa.png" alt="" width="40"></a>
	                       </ul>
						   
						</div>
					</div>
				</nav>
		</header>
		<section id="bottom">
			<div class="container-fluid px-4">
				<div class="row">
					<div class="col-lg-12 text-right">
						<a href="tel:5550003600" style="margin-right: 34px;color: #74868d;font-size: 19px;position: relative;top: -1em;z-index: 99;"><strong style="color: #fa222b;">Llámanos:</strong> (55) 5000 3600</a>
					</div>
				</div>
			</div>
		</section>
