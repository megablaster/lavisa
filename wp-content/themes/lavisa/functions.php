<?php
/**
 * lavisa functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package lavisa
 */

if ( ! function_exists( 'lavisa_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function lavisa_setup() {

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		 // Menu
	    if (!file_exists( get_template_directory() . '/utilities/navwalkerbs4.php')) {
	        return new WP_Error( 'navwalkerbs4-missing', __( 'It appears the navwalkerbs4.php file may be missing.', 'navwalkerbs4' ) );
	      } else {
	        require_once get_template_directory() . '/utilities/navwalkerbs4.php';
	      }


      register_nav_menus( array(
              'primary' => __( 'Main menu', 'amij' ),
      ) );

      
	}
endif;
add_action( 'after_setup_theme', 'lavisa_setup' );




    if( function_exists('acf_add_options_page') ) {
      
      acf_add_options_page(array(
        'page_title'  => 'Configuración lavisa',
        'menu_title'  => 'Configuración lavisa',
        'menu_slug'   => 'lazos-general-settings',
        'capability'  => 'edit_posts',
        'redirect'    => false
      ));
    }

add_post_type_support( 'page', 'excerpt' );


//Remove adminbar
add_filter('show_admin_bar', '__return_false');

?>


