<?php get_header();?>
<!-- <style>
	.altura-general {
		background-size: contain;
		background-position: right;
		background-repeat: no-repeat;
	}

	  /* Large Devices, Wide Screens */
    @media only screen and (max-width : 1200px) {

    }

    /* Medium Devices, Desktops */
    @media only screen and (max-width : 992px) {
    	.altura-general {
		    background-size: cover;
		    background-position: right;
		}

		#SitioLavisa .tamano-fuente-general {
		    padding: 0.5em;
		    background-color: rgba(255,255,255,0.7);
		    text-shadow: 1px 1px 1px #fff;
		}
    }

    /* Small Devices, Tablets */
    @media only screen and (max-width : 768px) {

    }

    /* Extra Small Devices, Phones */ 
    @media only screen and (max-width : 480px) {

    }

    /* Custom, iPhone Retina */ 
    @media only screen and (max-width : 320px) {

    }

</style> -->

<section id="homepage">
	
</section>

<section class="altura-general">
	<div class="container-fluid h-100">
		<div class="row h-100 align-items-center">
			<div class="col-xl-4 col-lg-6 col-md-12 px-5 col-sm-12 text-justify">
				<p class="text-muted tamano-fuente-general" style="font-size: 20px;">
					<?php the_field('texto_home', 'option'); ?>
				</p>
			</div>
			<div class="col-xl-8 col-lg-6" style="padding:0;">
				<img src="<?php the_field('image_home', 'option'); ?>" class="img-fluid">
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>