<footer id="footer" class="text-muted">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 pt-4 pb-3  px-5">
                <a href="<?php bloginfo('template_url');?>/mapa-de-sitio" class="text-muted">Mapa de sitio</a>
            </div>
            <div class="col-md-6 pt-4 pb-3 text-center">
                Copyright © <?php echo date('Y');?> LAVISA. Todos los derechos reservados
            </div>
            <div class="col-md-3 pt-3 pb-3 social text-right px-5">

                <?php 
                    if( have_rows('redes_sociales', 'option') ): 
                      while( have_rows('redes_sociales', 'option') ): the_row(); 
                      ?>

                    <a href="<?php the_sub_field('liga'); ?>" target="_blank">
                        <img src="<?php the_sub_field('imagen'); ?>" alt="">
                    </a>
                  

                <?php
                    endwhile; 
                    endif;
                    wp_reset_query();
                ?> 
            </div>
        </div>
    </div>
</footer>
<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/lib/framework.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/generals.min.js"></script>
<?php wp_footer(); ?>
</body>
</html>
