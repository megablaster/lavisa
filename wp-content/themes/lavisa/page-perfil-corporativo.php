<?php get_header();?>
<div class="container-fluid"> 
    <div class="owl-carousel owl-theme">
        <div class="item">
            <div class="row">
                <div class="offset-1 col-md-5 d-flex align-items-center">
                    <ul class="list-unstyled text-muted">
                            <li><h2>Productos</h2></li>
                            <li>Productos tales como: tubería de acero, tubería de perforación y 
                                revestimiento, válvulas, válvulas lanzadoras para limpieza de ductos, 
                                bridas, conexiones, juntas articuladas, espárragos, soportes de resorte, 
                                juntas y empaques, son expedidos cada día a todos los destinos. Ofrecemos 
                                asistencia técnica, ubicación, compra, almacenaje, garantía de calidad, 
                                pruebas metalúrgicas, certificación y logística de transporte como parte 
                                de nuestros servicios al cliente.</li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <img src="images/productores.png" class="img-fluid">
                </div>
            </div>
        </div>   
        <div class="item">
            <div class="row">
                <div class="offset-1 col-md-5 d-flex align-items-center">
                    <ul class="list-unstyled text-muted">
                            <li><h2>Clientes</h2></li>
                            <li>Con sede en la Ciudad de México, LAVISA es una 
                                empresa de clase mundial dedicada al suministro 
                                de materiales, productos y servicios relacionados 
                                con la industria del petróleo y gas.</li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <img src="images/planta.png" class="img-fluid">
                </div>
            </div>
        </div>
        <div class="item">
            <div class="row">
                <div class="offset-1 col-md-5 d-flex align-items-center">
                    <ul class="list-unstyled text-muted">
                            <li><h2>Oficina corporativa</h2></li>
                            <li>Con sede en la Ciudad de México, LAVISA es una 
                                empresa de clase mundial dedicada al suministro de 
                                materiales, productos y servicios relacionados con 
                                la industria del petróleo y gas</li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <img src="images/oficina_corporativa.png" class="img-fluid">
                </div>
            </div>
        </div>   
    </div>
</div>
<?php get_footer(); ?>
                    
