<?php get_header(); ?>
<style>
	#navbar-main {
		position: fixed;
		top:0;
		width: 100%;
		z-index: 10;
	}

	#footer {
		position: fixed;
	}
</style>
<section id="home" style="background-image: url('<?php the_field('image_home', 'option'); ?>');">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-5 offset-xl-1 col-lg-8 offset-lg-2 col-md-8 offset-md-2 col-sm-10 offset-sm-1 vh">
				<div class="text">
					<?php the_field('texto_home', 'option'); ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>