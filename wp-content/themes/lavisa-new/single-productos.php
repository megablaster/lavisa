<?php get_header(); ?>
<section id="single-products">

	<section id="body">
		<div class="container">
			<div class="row">
				<div class="col-xl-5">
					<img src="<?php the_post_thumbnail_url();?>" class="img-fluid img-cover" alt="<?php the_title(); ?>">
				</div>
				<div class="col-xl-6 offset-xl-1">
					<?php while(have_posts()): the_post() ?>
						<div class="text">
							<?php the_title('<h1>','</h1>'); ?>
							<?php the_content(); ?>
						</div>
					<?php endwhile ?>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="owl-carousel owl-theme" id="products-related">
					<?php 
						$args = array(
							'post_type' => 'productos',
							'posts_per_page' => -1
						);

						$q = new WP_Query($args);
					?>

					<?php while($q->have_posts()): $q->the_post() ?>

						<div class="item">
							<a href="<?php the_permalink();?>">
								<img src="<?php the_post_thumbnail_url(); ?>" class="img-fluid" alt="<?php the_title(); ?>">
							</a>
						</div>
					<?php endwhile ?>

				</div>
			</div>
		</div>
	</section>

</section>
<?php get_footer(); ?>