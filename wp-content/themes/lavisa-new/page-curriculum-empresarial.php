<?php get_header(); the_post(); ?>
<style>
	@media only screen and (max-width : 1200px) {
		#curriculum {
			background-image: url('<?php the_post_thumbnail_url();?>');
		}
	}
</style>

<section id="curriculum">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-7 p-r bg" style="background-image: url('<?php the_post_thumbnail_url();?>');">
			</div>
			<div class="col-xl-4">
				<div class="text">
					<?php the_content();?>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>