<?php get_header();  the_post(); ?>
<section id="corporativo">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12 sinpadding">
                <div class="owl-carousel owl-theme" id="slide-one">
                    
                    <?php 
                        if( have_rows('slide') ): 
                          while( have_rows('slide') ): the_row(); 
                          ?>

                           <div class="item" style="background-image: url('<?php the_sub_field('imagen'); ?>'); background-size: cover;">
                                <div class="text">
                                    <?php the_sub_field('texto'); ?>
                                </div>
                            </div>

                    <?php
                        endwhile; 
                        endif;
                        wp_reset_query();
                    ?> 

                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>