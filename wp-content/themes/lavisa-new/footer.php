<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;?>

<section id="footer">
	<div class="container-fluid max-width">
		<div class="row">
			<div class="col-lg-3 col-md-12">
				<p><a href="<?php echo home_url('/mapa-de-sitio');?>">Mapa del sitio</a></p>
			</div>
			<div class="col-lg-6 col-md-12 text-center">
				<p>Copyright © <?php date('Y'); ?> LAVISA. Todos los derechos reservados</p>
			</div>
			<div class="col-lg-3 col-md-12 text-right">
				<ul class="icons">

					<?php  if( have_rows('redes_sociales', 'option') ): ?>

	                    <?php while( have_rows('redes_sociales', 'option') ): the_row(); ?>

		                    <li>
							   <a href="<?php the_sub_field('liga'); ?>" target="_blank">
							   	<img src="<?php the_sub_field('imagen'); ?>" alt="icono" class="img-fluid">
							   </a>
							</li>
						<?php endwhile ?>
						
					<?php endif ?>
					
				</ul>
			</div>
		</div>
	</div>
</section>

</div>
<?php wp_footer(); ?>
</body>
</html>
