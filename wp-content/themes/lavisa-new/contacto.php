<?php
/* Template Name: Contacto
*/
get_header();  the_post();
?>
<section id="contacto">
    <div class="container-fluid">
        <div class="row bg-grey">
            <div class="col-xl-3 offset-xl-1">
                <div class="text">
                    <?php the_content();?>
                </div>
            </div>
            <div class="col-xl-7 offset-xl-1 p-r bg" style="background-image: url('<?php the_post_thumbnail_url();?>');"></div>
        </div>
        <div class="row">
            <div class="col-xl-6">
                 <div class="embed-responsive embed-responsive-16by9">
                    <?php the_field('mapa'); ?>
                </div>
            </div>
            <div class="col-xl-4 offset-xl-1">
                <div class="form2">
                    <?php echo do_shortcode(get_field('form')); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>