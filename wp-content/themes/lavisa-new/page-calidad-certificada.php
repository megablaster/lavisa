<?php get_header(); the_post(); ?>

<section id="certificada">
	<div class="container">
		<div class="row">
			<div class="row">
				<div class="col-md-12 col-sm-12 pt-3 pb-3 text-right">
					<img src="<?php echo get_stylesheet_directory_uri().'/img/calidad_logo.png';?>" class="img-fluid">
				</div>
				<div class="col-md-4 col-sm-12 text-justify">
					<h3>Política de Calidad <i><img src="<?php echo get_stylesheet_directory_uri().'/img/palomita_pequeña.png';?>"></i></h3>
					<p> <?php the_field('politica_de_calidad'); ?></p>
				</div>
				<div class="col-md-4 col-sm-12 text-justify">
					<h3>Compromiso <i><img src="<?php echo get_stylesheet_directory_uri().'/img/palomita_pequeña.png';?>"></i></h3>
					<p> <?php the_field('compromiso'); ?></p>
				</div>
				<div class="col-md-4 col-sm-12 text-justify">
					<h3>Tecnología <i><img src="<?php echo get_stylesheet_directory_uri().'/img/palomita_pequeña.png';?>"></i></h3>
					<p> <?php the_field('tecnologia'); ?></p>
					<h3>Infraestructura <i><img src="<?php echo get_stylesheet_directory_uri().'/img/palomita_pequeña.png';?>"></i></h3>
					<p> <?php the_field('infraestructura'); ?></p>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>