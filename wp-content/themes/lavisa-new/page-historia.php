<?php get_header(); the_post(); ?>

<section id="historia">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<?php the_content();?>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>