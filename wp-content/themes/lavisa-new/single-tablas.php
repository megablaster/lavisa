<?php get_header();  ?>
<section id="table">
	<section id="header">
		<div class="container">
			<div class="row">
				<div class="col-xl-12 text-center">
					<h2><?php the_title(); ?></h2>
				</div>
			</div>
		</div>	
	</section>
	<div class="container" id="body">
		<div class="row">
			
			<?php if( have_rows('tablas') ): ?>
				<div class="col-xl-12 text-center">
					<?php while( have_rows('tablas') ): the_row();?>
						<img src="<?php the_sub_field('imagen_tabla'); ?>"  class="img-fluid">
					<?php endwhile ?>
				</div>
			<?php endif ?>
			
		</div>
	</div>
</section>
<?php get_footer(); ?>