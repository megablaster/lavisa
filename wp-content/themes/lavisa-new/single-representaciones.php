<?php get_header();  the_post(); ?>
<section id="representaciones">
    <div class="container-fluid">
        <div class="row">

            <div class="col-xl-4 offset-xl-1">

                <div class="vertical">
                    <div class="text">
                        <h3><strong><?php the_title();?></strong></h3>
                        <div class="scroll">
                            <?php the_content(); ?>
                        </div>
                    </div>

                    <ul class="list-unstyled">
                        <h3>Catálogos</h3>
                        <?php if( have_rows('catalogos') ): ?>

                            <div class="catalog-list">
                                <?php while( have_rows('catalogos') ): the_row() ?>

                                    <?php if( have_rows('lista') ): ?>
                                        <?php while( have_rows('lista') ): the_row() ?>
                                            <li class="pdf">
                                                <?php if (get_sub_field('liga')): ?>
                                                    <a data-fancybox data-type="iframe" data-src="<?php the_sub_field('liga'); ?>" href="javascript:;" class="text-muted"><?php the_sub_field('nombre_catalogo'); ?> 
                                                        <img src="<?php echo get_stylesheet_directory_uri().'/img/download.png';?>" alt="download" class="img-fluid download">
                                                    </a>
                                                <?php else: ?>
                                                    <a href="javascript:;" class="text-muted"><?php the_sub_field('nombre_catalogo'); ?> </a>
                                                <?php endif ?>
                                                
                                            </li>
                                        <?php endwhile ?>
                                    <?php endif ?>
                            
                                <?php endwhile ?>
                            </div>
                        <?php endif ?>

                        <?php wp_reset_query(); ?>
                    </ul>
                </div>

            </div>
            <div class="col-xl-7" style="position: relative;">
                <img src="<?php the_field('imagen'); ?>" class="img-fluid">
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>