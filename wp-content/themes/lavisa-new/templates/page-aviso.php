<?php
/* Template Name: Aviso Privacidad
*/ 

get_header(); 

the_post();

?>
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <img src="<?php bloginfo('template_url');?>/images/aviso-privacidad.png" class="img-fluid">
        </div>
    </div>
</div>
<?php get_footer(); ?>