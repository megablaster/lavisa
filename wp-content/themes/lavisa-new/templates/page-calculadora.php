<?php
/* Template Name: calculadora
*/ 

get_header(); 

the_post();

?>
<section class="no-gutters">
    <div class="container-fluid text-center my-5">
        <h1 class="y-5"><?php the_title();?></h1>
        <div class="row align-items-center d-flex justify-content-center">
            <div class="col-md-6 mt-5">                
                        <h2> longitud</h2>
                    <p>
                                    <!--Calculator Begins Here -->
                          <div id="ConvertionFormulas"><!-- LENGTH CONVERTER START -->
                        <form style="padding:0;margin:0" name="cat=Length;focuscolor=blue;focusweight=bold">
                        <table align=center width=800 style="font-family:Arial;font-size:100%" border=0 cellpadding=0 cellspacing=0>
                          <tr>
                            <td style="padding:1.2ex 1.5ex 0.2ex;width:50%;font-family:Arial;font-size:13px">&nbsp;</td>
                            <td style="padding:1.2ex 1.5ex 0.2ex;width:50%;font-family:Arial;font-size:13px"><span class="style1">A continuación ingrese cantidades a convertir:</span></td>
                          <tr><td width="16%" style="padding:1.2ex 1.5ex 0.2ex;width:50%;font-family:Arial;font-size:13px"><div align="right" class="h12"><span name="inch" id="inch">Pulgadas [in]:</span></div></td>
                        <td width="84%" style="padding:1.2ex 1.5ex 0.2ex;width:50%;font-family:Arial;font-size:13px"><input style="width:100%;font-family:Arial;font-size:100%" type=text name="inch;base=1" class="h12"></td><tr><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><div align="right" class="h13"><span name="meter" id="meter">Metros [m]:</span></div></td><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><input style="width:100%;font-family:Arial;font-size:100%" type=text name="meter" class="h13"></td><tr><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><div align="right" class="h12"><span name="kilometer" id="kilometer">Kilómetros [km]:</span></div></td><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><input style="width:100%;font-family:Arial;font-size:100%" type=text name="kilometer" class="h12"></td><tr><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><div align="right" class="h13"><span name="centimeter" id="centimeter">Centímetros [cm]:</span></div></td><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><input style="width:100%;font-family:Arial;font-size:100%" type=text name="centimeter" class="h13"></td><tr><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><div align="right" class="h12"><span name="millimeter" id="millimeter">Miímetros [mm]:</span></div></td><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><input style="width:100%;font-family:Arial;font-size:100%" type=text name="millimeter" class="h12"></td><tr><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><div align="right" class="h13"><span name="mile" id="mile">Millas [mi, mi(Int)]:</span></div></td><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><input style="width:100%;font-family:Arial;font-size:100%" type=text name="mile" class="h13"></td><tr><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><div align="right" class="h12"><span name="yard" id="yard">Yardas [yd]:</span></div></td><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><input style="width:100%;font-family:Arial;font-size:100%" type=text name="yard" class="h12"></td><tr><td style="padding:0.2ex 1.5ex 1.2ex;width:50%;font-family:Arial;font-size:13px"><div align="right"class="h13"><span name="foot" id="foot">Pies [ft]:</span></div></td><td style="padding:0.2ex 1.5ex 1.2ex;width:50%;font-family:Arial;font-size:13px"><input style="width:100%;font-family:Arial;font-size:100%" type=text name="foot" class="h13"></td></table>
                        <p>&nbsp;</p>
                        </form>
                        <script src=http://www.unitconversion.org/converter3/converter3.js></script>
                        <!-- LENGTH CONVERTER END -->

                        <h2> peso</h2>
                        <!-- WEIGHT CONVERTER START -->
                        <form style="padding:0;margin:0" name="cat=Weight;focuscolor=blue;focusweight=bold">
                        <table align=center width=800 style="font-family:Arial;font-size:100%" border=0 cellpadding=0 cellspacing=0>
                          <tr>
                            <td style="padding:1.2ex 1.5ex 0.2ex;width:50%;font-family:Arial;font-size:13px">&nbsp;</td>
                            <td style="padding:1.2ex 1.5ex 0.2ex;width:50%;font-family:Arial;font-size:13px"><span class="style1">A continuación ingrese cantidades a convertir:</span></td>
                          <tr><td style="padding:1.2ex 1.5ex 0.2ex;width:50%;font-family:Arial;font-size:13px"><div align="right" class="h12"><span name="kilogram" id="kilogram">Kilogramo [kg]:</span></div></td><td style="padding:1.2ex 1.5ex 0.2ex;width:50%;font-family:Verdana;font-size:13px"><input style="width:100%;font-family:Verdana;font-size:100%" type=text name="kilogram;base=1""class="h12"></td><tr><td style="padding:0.2ex 1.5ex;width:50%;font-family:Verdana;font-size:13px"><div align="right" class="h13"><span name="gram" id="gram">Gramo [g]:</span></div></td><td style="padding:0.2ex 1.5ex;width:50%;font-family:Verdana;font-size:13px"><input style="width:100%;font-family:Verdana;font-size:100%" type=text name="gram""class="h13"></td><tr><td style="padding:0.2ex 1.5ex;width:50%;font-family:Verdana;font-size:13px"><div align="right" class="h12"><span name="milligram" id="milligram">Milligramo [mg]:</span></div></td><td style="padding:0.2ex 1.5ex;width:50%;font-family:Verdana;font-size:13px"><input style="width:100%;font-family:Verdana;font-size:100%" type=text name="milligram""class="h12"></td><tr><td style="padding:0.2ex 1.5ex;width:50%;font-family:Verdana;font-size:13px"><div align="right" class="h13"><span name="kilopound" id="kilopound">Kilo-Libra [kip]:</span></div></td><td style="padding:0.2ex 1.5ex;width:50%;font-family:Verdana;font-size:13px"><input style="width:100%;font-family:Verdana;font-size:100%" type=text name="kilopound""class="h13"></td><tr><td style="padding:0.2ex 1.5ex;width:50%;font-family:Verdana;font-size:13px"><div align="right" class="h12"><span name="pound" id="pound">Libra [lb]:</span></div></td><td style="padding:0.2ex 1.5ex;width:50%;font-family:Verdana;font-size:13px"><input style="width:100%;font-family:Verdana;font-size:100%" type=text name="pound""class="h12"></td><tr><td style="padding:0.2ex 1.5ex;width:50%;font-family:Verdana;font-size:13px"><div align="right" class="h13"><span name="ounce" id="ounce">Onza [oz]:</span></div></td><td style="padding:0.2ex 1.5ex;width:50%;font-family:Verdana;font-size:13px"><input style="width:100%;font-family:Verdana;font-size:100%" type=text name="ounce""class="h13"></td><tr><td style="padding:0.2ex 1.5ex;width:50%;font-family:Verdana;font-size:13px"><div align="right" class="h12">
                          <div align="right" class="h12"><span name="ton (short)" id="ton (short)">Tonelada  (corta) [ton (US)]:</span></div></td><td style="padding:0.2ex 1.5ex;width:50%;font-family:Verdana;font-size:13px"><input style="width:100%;font-family:Verdana;font-size:100%" type=text name="ton (short)""class="h12"></td><tr><td style="padding:0.2ex 1.5ex;width:50%;font-family:Verdana;font-size:13px"><div align="right" class="h13"><span name="ton (assay) (US)" id="ton (assay) (US)">Tonelada (assay) (US) [AT (US)]:</span></div></td><td style="padding:0.2ex 1.5ex;width:50%;font-family:Verdana;font-size:13px"><input style="width:100%;font-family:Verdana;font-size:100%" type=text name="ton (assay) (US)""class="h13"></td><tr><td style="padding:0.2ex 1.5ex 1.2ex;width:50%;font-family:Verdana;font-size:13px"><div align="right" class="h12"><span name="ton (long)" id="ton (long)">Tonelada (long) [ton (UK), t (long)]:</span></div></td><td style="padding:0.2ex 1.5ex 1.2ex;width:50%;font-family:Verdana;font-size:13px"><input style="width:100%;font-family:Verdana;font-size:100%" type=text name="ton (long)""class="h12"></td></table>
                        <p>&nbsp;</p>
                        </form>
                        <script src=http://www.unitconversion.org/converter3/converter3.js></script>
                        <!-- WEIGHT CONVERTER END -->


                        <h2> temperatura</h2>

                        <!-- TEMPERATURE CONVERTER START -->
                        <form style="padding:0;margin:0" name="cat=Temperature;focuscolor=blue;focusweight=bold">
                        <table align=center width=800 style="font-family:Arial;font-size:100%" border=0 cellpadding=0 cellspacing=0>
                          <tr>
                            <td style="padding:1.2ex 1.5ex 0.2ex;width:50%;font-family:Arial;font-size:13px">&nbsp;</td>
                            <td style="padding:1.2ex 1.5ex 0.2ex;width:50%;font-family:Arial;font-size:13px"><span class="style1">A continuación ingrese cantidades a convertir:</span></td>
                          <tr><td style="padding:1.2ex 1.5ex 0.2ex;width:50%;font-family:Arial;font-size:13px"><div align="right" class="h12"><span name="kelvin" id="kelvin">Grados Kelvin [K]:</span></div></td><td style="padding:1.2ex 1.5ex 0.2ex;width:50%;font-family:Arial;font-size:13px"><input style="width:100%;font-family:Arial;font-size:100%" type=text name="kelvin;base=1"class="h12"></td><tr><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><div align="right"class="h13"><span name="degree Celsius" id="degree Celsius">Grados Celsius [&#176;C]:</span></div></td><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><input style="width:100%;font-family:Arial;font-size:100%" type=text name="degree Celsius"class="h13"></td><tr><td style="padding:0.2ex 1.5ex 1.2ex;width:50%;font-family:Arial;font-size:13px"><div align="right"class="h12"><span name="degree Fahrenheit" id="degree Fahrenheit">Grados Fahrenheit [&#176;F]:</span></div></td><td style="padding:0.2ex 1.5ex 1.2ex;width:50%;font-family:Arial;font-size:13px"><input style="width:100%;font-family:Arial;font-size:100%" type=text name="degree Fahrenheit"class="h12"></td></table>
                        <p>&nbsp;</p>
                        </form>
                        <script src=http://www.unitconversion.org/converter3/converter3.js></script>
                        <!-- TEMPERATURE CONVERTER END -->

                        <h2> area</h2>

                        <!-- AREA CONVERTER START -->
                        <form style="padding:0;margin:0" name="cat=Area;focuscolor=blue;focusweight=bold">
                        <table align=center width=800 style="font-family:Arial;font-size:100%" border=0 cellpadding=0 cellspacing=0>
                          <tr>
                            <td style="padding:1.2ex 1.5ex 0.2ex;width:50%;font-family:Arial;font-size:13px">&nbsp;</td>
                            <td style="padding:1.2ex 1.5ex 0.2ex;width:50%;font-family:Arial;font-size:13px"><span class="style1">A continuación ingrese cantidades a convertir:</span></td>
                          <tr><td style="padding:1.2ex 1.5ex 0.2ex;width:50%;font-family:Arial;font-size:13px"><div align="right"class="h12"><span name="square meter" id="square meter">Metro Cuadrado [m<sup>2</sup>]:</span></div></td><td style="padding:1.2ex 1.5ex 0.2ex;width:50%;font-family:Arial;font-size:13px"><input style="width:100%;font-family:Arial;font-size:100%" type=text name="square meter;base=1"class="h12"></td><tr><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><div align="right"class="h13"><span name="square kilometer" id="square kilometer">Kilómetro Cuadrado [km<sup>2</sup>]:</span></div></td><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><input style="width:100%;font-family:Arial;font-size:100%" type=text name="square kilometer"class="h13"></td><tr><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><div align="right"class="h12"><span name="square millimeter" id="square millimeter">Milímetro Cuadrado [mm<sup>2</sup>]:</span></div></td><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><input style="width:100%;font-family:Arial;font-size:100%" type=text name="square millimeter"class="h12"></td><tr><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><div align="right"class="h13"><span name="square centimeter" id="square centimeter">Centímetro Cuadrado [cm<sup>2</sup>]:</span></div></td><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><input style="width:100%;font-family:Arial;font-size:100%" type=text name="square centimeter"class="h13"></td><tr><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><div align="right"class="h12"><span name="hectare" id="hectare">Hectárea [ha]:</span></div></td><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><input style="width:100%;font-family:Arial;font-size:100%" type=text name="hectare"class="h12"></td><tr><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><div align="right"class="h13"><span name="square mile" id="square mile">Milla Cuadrada [mi<sup>2</sup>]:</span></div></td><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><input style="width:100%;font-family:Arial;font-size:100%" type=text name="square mile"class="h13"></td><tr><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><div align="right"class="h12"><span name="square yard" id="square yard">Yarda Cuadrada [yd<sup>2</sup>]:</span></div></td><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><input style="width:100%;font-family:Arial;font-size:100%" type=text name="square yard"class="h12"></td><tr><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><div align="right"class="h13"><span name="square foot" id="square foot">Pie Cuadrado [ft<sup>2</sup>]:</span></div></td><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><input style="width:100%;font-family:Arial;font-size:100%" type=text name="square foot"class="h13"></td><tr><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><div align="right"class="h12"><span name="square inch" id="square inch">Pulgada Cuadrado [in<sup>2</sup>]:</span></div></td><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><input style="width:100%;font-family:Arial;font-size:100%" type=text name="square inch"class="h12"></td><tr><td style="padding:0.2ex 1.5ex 1.2ex;width:50%;font-family:Arial;font-size:13px"><div align="right"class="h13"><span name="acre" id="acre">Acre [ac]:</span></div></td><td style="padding:0.2ex 1.5ex 1.2ex;width:50%;font-family:Arial;font-size:13px"><input style="width:100%;font-family:Arial;font-size:100%" type=text name="acre"class="h13"></td></table>
                        <p>&nbsp;</p>
                        </form>
                        <script src=http://www.unitconversion.org/converter3/converter3.js></script>
                        <!-- AREA CONVERTER END -->

                        <h2> presion</h2>

                        <!-- PRESSURE CONVERTER START -->
                        <form style="padding:0;margin:0" name="cat=Pressure;focuscolor=blue;focusweight=bold">
                        <table align=center width=800 style="font-family:Arial;font-size:100%" border=0 cellpadding=0 cellspacing=0>
                          <tr>
                            <td style="padding:1.2ex 1.5ex 0.2ex;width:50%;font-family:Arial;font-size:13px">&nbsp;</td>
                            <td style="padding:1.2ex 1.5ex 0.2ex;width:50%;font-family:Arial;font-size:13px"><span class="style1">A continuación ingrese cantidades a convertir:</span></td>
                          <tr><td style="padding:1.2ex 1.5ex 0.2ex;width:50%;font-family:Arial;font-size:13px"><div align="right"class="h12"><span name="bar" id="bar">Bar:</span></div></td><td style="padding:1.2ex 1.5ex 0.2ex;width:50%;font-family:Arial;font-size:13px"><input style="width:100%;font-family:Arial;font-size:100%" type=text name="bar;base=1"class="h12"></td><tr><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><div align="right"class="h13"><span name="kilogram-force/square centimeter" id="kilogram-force/square centimeter">Kilogramo Fuerza/Centímetro Cuadrado [kgf/cm<sup>2</sup>]:</span></div></td><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><input style="width:100%;font-family:Arial;font-size:100%" type=text name="kilogram-force/square centimeter"class="h13"></td><tr><td style="padding:0.2ex 1.5ex 1.2ex;width:50%;font-family:Arial;font-size:13px"><div align="right"class="h12"><span name="psi" id="psi">PSI [psi]:</span></div></td><td style="padding:0.2ex 1.5ex 1.2ex;width:50%;font-family:Arial;font-size:13px"><input style="width:100%;font-family:Arial;font-size:100%" type=text name="psi"class="h12"></td></table>
                        <p>&nbsp;</p>
                        </form>
                        <script src=http://www.unitconversion.org/converter3/converter3.js></script>
                        <!-- PRESSURE CONVERTER END -->

                        <h2> Volumen</h2>

                        <!-- VOLUME CONVERTER START -->
                        <form style="padding:0;margin:0" name="cat=Volume;focuscolor=blue;focusweight=bold">
                        <table align=center width=800 style="font-family:Arial;font-size:100%" border=0 cellpadding=0 cellspacing=0>
                          <tr>
                            <td style="padding:1.2ex 1.5ex 0.2ex;width:50%;font-family:Arial;font-size:13px">&nbsp;</td>
                            <td style="padding:1.2ex 1.5ex 0.2ex;width:50%;font-family:Arial;font-size:13px"><span class="style1">A continuación ingrese cantidades a convertir:</span></td>
                          <tr><td style="padding:1.2ex 1.5ex 0.2ex;width:50%;font-family:Arial;font-size:13px"><div align="right"class="h12"><span name="quart (US)" id="quart (US)">Quart (US) [qt (US), qt liq (US)]:</span></div></td><td style="padding:1.2ex 1.5ex 0.2ex;width:50%;font-family:Arial;font-size:13px"><input style="width:100%;font-family:Arial;font-size:100%" type=text name="quart (US);base=1"class="h12"></td><tr><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><div align="right"class="h13"><span name="liter" id="liter">Litro [L, l]:</span></div></td>
                            <td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><input style="width:100%;font-family:Arial;font-size:100%" type="text" name="liter"class="h13" /></td>
                        <tr><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><div align="right"class="h12"><span name="barrel (oil)" id="barrel (oil)">Barril (oil) [bbl (oil), bbl (petroleum)]:</span></div></td><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><input style="width:100%;font-family:Arial;font-size:100%" type=text name="barrel (oil)"class="h12"></td><tr><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><div align="right"class="h13"><span name="barrel (US)" id="barrel (US)">Barril (US) [bbl (US), bbl liq (US), bbl fluid, fl-bbl]:</span></div></td><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><input style="width:100%;font-family:Arial;font-size:100%" type=text name="barrel (US)"class="h13"></td><tr><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><div align="right"class="h12"><span name="gallon (US)" id="gallon (US)">Galón (US) [gal (US), gal liq (US), gal]:</span></div></td><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><input style="width:100%;font-family:Arial;font-size:100%" type=text name="gallon (US)"class="h12"></td><tr><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><div align="right"class="h13"><span name="pint (US)" id="pint (US)">Pint (US) [pt (US), pt liq (US)]:</span></div></td><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><input style="width:100%;font-family:Arial;font-size:100%" type=text name="pint (US)"class="h13"></td><tr><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><div align="right"class="h12"><span name="cup (US)" id="cup (US)">Cup (US):</span></div></td><td style="padding:0.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px"><input style="width:100%;font-family:Arial;font-size:100%" type=text name="cup (US)"class="h12"></td><tr><td style="padding:0.2ex 1.5ex 1.2ex;width:50%;font-family:Arial;font-size:13px"><div align="right"class="h13"><span name="fluid ounce (US)" id="fluid ounce (US)">Onza Liquida (US) [fl oz (US)]:</span></div></td><td style="padding:0.2ex 1.5ex 1.2ex;width:50%;font-family:Arial;font-size:13px"><input style="width:100%;font-family:Arial;font-size:100%" type=text name="fluid ounce (US)"class="h13"></td></table>

                        </span>
                        </form>
                        <script src=http://www.unitconversion.org/converter3/converter3.js></script>
                        <!-- VOLUME CONVERTER END -->

                        <h2> Fuerza</h2>

                        <!-- FORCE CONVERTER START -->
                        <form style="padding:0;margin:0" name="cat=Force;focuscolor=blue;focusweight=bold">
                        <table align=center width=800 style="font-family:Verdana;font-size:100%" border=0 cellpadding=0 cellspacing=0><tr><td style="padding:1.2ex 1.5ex 0.2ex;width:50%;font-family:Verdana;font-size:13px"><div align="right"class="h12"><span name="kilonewton" id="kilonewton">kilonewton [kN]:</span></div></td><td style="padding:1.2ex 1.5ex 0.2ex;width:50%;font-family:Verdana;font-size:13px"><input style="width:100%;font-family:Verdana;font-size:100%" type=text name="kilonewton;base=1""class="h12"></td><tr><td style="padding:0.2ex 1.5ex 1.2ex;width:50%;font-family:Verdana;font-size:13px"><div align="right"class="h13"><span name="pound-force" id="pound-force">pound-force [lbf]:</span></div></td><td style="padding:0.2ex 1.5ex 1.2ex;width:50%;font-family:Verdana;font-size:13px"><input style="width:100%;font-family:Verdana;font-size:100%" type=text name="pound-force" class="h13"></td></table>
                        </form>
                        <script src=http://www.unitconversion.org/converter3/converter3.js></script>
                        <!-- FORCE CONVERTER END -->
                        <!-- FUERZA CONVERTER END -->
                        <a href="http://www.unitconversion.org">UnitConversion.org</a> - the ultimate unit conversion resource.




                        <script src=http://www.unitconversion.org/converter3/converter3.js async></script>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>