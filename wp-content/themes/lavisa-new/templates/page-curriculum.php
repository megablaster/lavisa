<?php
/* Template Name: Curriculum
*/ 

get_header(); 

the_post();

?>
<section class="altura-general">
    <div class="container-fluid h-100">
        <div class="row">
            <div class="col-md-7 col-sm-12 no-gutters">
                <?php 
                    if (has_post_thumbnail()){ 
                    $imagen = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID),'full'); 
                    if (!empty($imagen)){
                            $src = $imagen['0'];
                    }else{
                        $src = get_template_directory_uri() . '/img/no-image.jpg';
                    }
                } ?>
                <img src="<?php echo $src; ?>" class="img-fluid">
            </div>
            <div class="col-md-4 col-sm-12 d-flex align-items-center pl-3 pr-3 no-gutters text-muted">
                 <?php the_content(); ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>