<?php get_header(); ?>
<section id="calidad" class="page-padding">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-3 offset-xl-1">
                <img src="<?php the_field('imagen_principal'); ?>" alt="Calidad lavisa" class="img-fluid img-vertical">
            </div>
            <div class="col-xl-8 p-r">

                <div class="owl-carousel owl-theme" id="slide-one">
                    <div class="item">
                        <img src="<?php the_field('imagen_secundaria'); ?>" class="img-fluid">
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
