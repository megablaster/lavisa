<?php
/* Template Name: Historia
*/ 

get_header(); 

the_post();

?>
<section class="altura-general">
	<div class="container h-100 d-flex align-items-center justify-content-center text-center">
		<div class="row">
			<div class="col-md-12 col-sm-12  pt-3 pb-3 text-center">
				<h1>Historia</h1>
				<img src="<?php the_field('imagen_historia'); ?>" alt="Historia Lavisa" class="img-fluid">
			</div>
		
		</div>
	</div>
</section>
<?php get_footer(); ?>