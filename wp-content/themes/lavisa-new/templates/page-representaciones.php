<?php
/* Template Name: Representaciones
*/ 

get_header(); 

the_post();

?>
<section class="altura-general fondo-patron">
    <div class="container-fluid h-100">
        <div class="row h-100 align-items-center justify-content-center text-muted">
            <div class="col-md-4 px-5">
                <?php the_content(); ?>
                   
                <ul class="list-unstyled">
                    <?php 
                        if( have_rows('catalogos') ): 
                          while( have_rows('catalogos') ): the_row(); 
                          ?>
                             <li><h3><?php the_sub_field('nombre'); ?></h3></li>

                                <?php 
                                    if( have_rows('lista') ): 
                                      while( have_rows('lista') ): the_row(); 
                                      ?>
                                        
                                        <li>
                                            <a href="<?php the_sub_field('liga'); ?>" class="text-muted"><?php the_sub_field('nombre_catalogo'); ?> 
                                                <img src="<?php bloginfo('template_url');?>/images/iconos_descarga.png" alt="" class="img-fluid pr-2 pt-2 pb-2" width="25">
                                            </a>
                                        </li>

                                       <?php
                                    endwhile; 
                                    endif;
                                    wp_reset_query();
                                ?> 
                      
                    <?php
                        endwhile; 
                        endif;
                        wp_reset_query();
                    ?>    
                        
                        
                </ul>
            </div>
                <?
                    if (has_post_thumbnail()){ 
                    $imagen = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID),'full'); 
                    if (!empty($imagen)){
                            $src = $imagen['0'];
                    }else{
                        $src = get_template_directory_uri() . '/img/no-image.jpg';
                    }
                } ?>


            <div class="col-md-8 h-100" style="background: url('<?php echo $src; ?>') no-repeat 100%; background-size: 100% 100%;"> </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>