<?php get_header(); ?>
<section class="page-padding" id="pruebas">
    <div class="container">
        <div class="row text-center">
            <div class="col-xl-12">
                <h1><?php the_title(); ?></h1>
            </div>
        </div>
        <div class="row">
            <?php 
                if( have_rows('prueba') ): 
                  while( have_rows('prueba') ): the_row(); 
                  ?>

                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 mt-4 prueba">
                    <img src="<?php the_sub_field('imagen_prueba'); ?>" alt="" class="img-fluid">
                </div>

           <?php
                endwhile; 
                endif;
                wp_reset_query();
            ?> 
        </div>
    </div>
</section>
<?php get_footer(); ?>