<?php get_header(); ?>
<section id="certificacion" class="page-padding">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-3 offset-xl-1">
                <img src="<?php the_field('imagen_principal'); ?>" class="img-fluid img-vertical">
            </div>
            <div class="col-xl-8 p-r">

                <div class="owl-carousel owl-theme" id="slide-one">
                    <?php 
                        if( have_rows('carousel') ): 
                          while( have_rows('carousel') ): the_row(); 
                          ?>

                      <div class="item">
                        <img src="<?php the_sub_field('imagen'); ?>" class="img-fluid">
                    </div>

                    <?php
                        endwhile; 
                        endif;
                        wp_reset_query();
                    ?> 
                </div>

            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>