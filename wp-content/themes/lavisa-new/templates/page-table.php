<?php
/* Template Name: Tablas
*/ 

get_header(); 

the_post();

?>
	<section class="container-fluid fondo-table">
		<div class="row">
			<div class="col-md-12 col-sm-12 text-center text-muted py-5">
				<h2><?php the_title(); ?></h2>
			</div>

		<?php 
			if( have_rows('tabla') ): 
				while( have_rows('tabla') ): the_row(); 
		?>

			<div class="col-md-12 col-sm-12 text-center d-block d-sm-none">
				<a href="<?php the_sub_field('archivo'); ?>" target="_blank" class="text-muted">Descargar Tabla
					 <img src="<?php bloginfo('template_url');?>/images/iconos_descarga.png" alt="" class="img-fluid pr-2 pt-2 pb-2" width="25">
                </a>


			</div>

			<div class="col-md-12 col-sm-12 text-center fondo-table-back py-5">
					<img src="<?php the_sub_field('imagen_tabla'); ?>" alt=""  class="img-fluid">
			</div>
		<?php
		    endwhile; 
		    endif;
		    wp_reset_query();
		?> 
		</div>
	</section>
<?php get_footer(); ?>