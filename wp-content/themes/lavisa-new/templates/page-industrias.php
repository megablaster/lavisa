<?php
/* Template Name: Sectores
*/ 

get_header(); 

the_post();

?>
<section id="section-industrias" class="fondo-patron-industrias altura-general">
    <div class="container-fluid h-100">
        <div class="row h-100 align-items-center justify-content-center text-muted">
            <div class="col-md-4 px-5 col-sm-12 text-muted no-gutters pl-5">
                <div class="pl-5">
                    <?php the_content(); ?>
                </div>
                <div class="estiloses pl-5">
                    <?php the_excerpt(); ?>       
                </div>  
            </div>

                    <?php 
                    if (has_post_thumbnail()){ 
                    $imagen = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID),'full'); 
                    if (!empty($imagen)){
                            $src = $imagen['0'];
                    }else{
                        $src = get_template_directory_uri() . '/img/no-image.jpg';
                    }
                } ?>

            <div class="col-md-8 col-sm-12 h-100 text-right no-gutters" style="background: url('<?php echo $src; ?>') no-repeat; background-size: cover;"></div>
        </div>
    </div>
</section>
<?php get_footer(); ?>