<?php get_header(); the_post(); ?>

<style>
    @media only screen and (max-width : 1200px) {
        #sector {
            background-image: url('<?php the_post_thumbnail_url();?>');
        }
    }
</style>

    <section id="sector">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-4 offset-xl-1 col-lg-10 offset-lg-1">
                    <div class="text">
                        <?php the_title('<h2>','</h2>'); ?><br>
                        <?php the_content(); ?>
                        <div class="triangle">
                            <?php the_field('descripcion_corta'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-xl-7 p-r bg" style="background-image: url('<?php the_post_thumbnail_url();?>');">
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>