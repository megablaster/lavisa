// jQuery('#loader')
jQuery( window ).load(function() {
  setTimeout(function(){
        jQuery('#loader').fadeOut();
    }, 1200);
});

jQuery(document).ready(function(){

    jQuery('#products-related').owlCarousel({
        loop:true,
        margin:40,
        nav:false,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        }
    });

    jQuery('#slide-one').owlCarousel({
        loop:true,
        margin:0,
        nav:false,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:false,
        dots:false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    });

    jQuery(window).scroll(function(){
        if (jQuery(this).scrollTop() > 50) {
           jQuery('#navbar-main').addClass('fixed');
        } else {
           jQuery('#navbar-main').removeClass('fixed');
        }
    });

    

});